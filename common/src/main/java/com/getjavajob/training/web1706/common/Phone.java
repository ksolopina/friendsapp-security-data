package com.getjavajob.training.web1706.common;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "phone_tbl")
public class Phone implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "phone_id", unique = true, nullable = false)
    private int id;
    @Column(name = "phone", nullable = false)
    private String phone;

    @ManyToOne
    @JoinColumn(name = "account_id", nullable = false)
    private Account account;

    public Phone() {
    }

    public Phone(String phone, Account account) {
        this.phone = phone;
        this.account = account;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "id=" + id +
                ", phone='" + phone + '\'' +
                ", account=" + account +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Phone phone1 = (Phone) o;

        if (!phone.equals(phone1.phone)) return false;
        return account.equals(phone1.account);
    }

    @Override
    public int hashCode() {
        int result = phone.hashCode();
        result = 31 * result + account.hashCode();
        return result;
    }

}
