package com.getjavajob.training.web1706.utils;

import com.getjavajob.training.web1706.common.Account;
import com.getjavajob.training.web1706.common.DTO.AccountDTO;
import org.apache.commons.codec.binary.Base64;

import javax.servlet.ServletException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Queue;

public class Utils {
    public static byte[] convertInputStreamToBytes(InputStream is) throws IOException {
        try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
            byte[] buffer = new byte[0xFFFF];
            for (int len; (len = is.read(buffer)) != -1; )
                os.write(buffer, 0, len);
            os.flush();
            return os.toByteArray();
        }
    }

    public static Date convertStringToDate(String date) throws ParseException {
        Date result = null;
        if (date != null && !date.equals("")) {
            DateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            result = format.parse(date);
        }
        return result;

    }

    public static String getEncodedFile(byte[] avatar) throws ServletException {
        if (avatar != null && avatar.length > 0) {
            try {
                return new String(Base64.encodeBase64(avatar), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                throw new ServletException(e);
            }
        }
        return null;
    }

    public static List<AccountDTO> getListAccountDTO (List<Account> accounts) throws ServletException {
        List<AccountDTO> accountsDTO = new ArrayList<>();
        for (Account account : accounts) {
            AccountDTO accountDTO = new AccountDTO();
            accountDTO.setId(account.getId());
            accountDTO.setFirstName(account.getFirstName());
            accountDTO.setLastName(account.getLastName());
            accountDTO.setEmail(account.getEmail());
            accountDTO.setAvatar(getEncodedFile(account.getAvatar()));
            accountsDTO.add(accountDTO);
        }
        return accountsDTO;
    }
}
