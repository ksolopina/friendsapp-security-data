                </div>
            </div>
            <footer class="footer">
                <div class="inner-container">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-8 col-sm-offset-2 text">
                                <p>© 2017</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
        <script>var ctx = '${pageContext.request.contextPath}';</script>
        <script src="${pageContext.request.contextPath}/js/main.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/friendsPage.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/search.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/pagination.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/uploadXML.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/download.js" type="text/javascript"></script>
    </body>
</html>