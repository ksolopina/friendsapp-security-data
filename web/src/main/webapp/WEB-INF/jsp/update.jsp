<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="common/header.jsp" %>
    <div class="row">
        <div class="update-form">
            <div class="form-box" >
                <div class="form-bottom update">
                    <form id="form-update" role="form" action="/doUpdate?${_csrf.parameterName}=${_csrf.token}" method="post" class="update-form" enctype="multipart/form-data">
                        <div class="control-group avatar-block">
                            <div class="profile-picture avatar-small">
                                <c:choose>
                                    <c:when test="${account.avatar != null}">
                                        <img class="editable img-responsive avatar" src="data:image/jpeg;base64,${sessionScope.avatar}"/>
                                    </c:when>
                                    <c:otherwise>
                                        <img src="${pageContext.request.contextPath}/img/avatar.png"
                                             class="editable img-responsive avatar">
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="avatar">Фото </label>
                            <div class="controls">
                                <input type="file" id="avatar" name="avatar" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="form-first-name">Имя</label>
                            <input type="text" name="firstName" value="${account.firstName}" class="form-first-name form-control" id="form-first-name">
                        </div>
                        <div class="form-group">
                            <label for="form-last-name">Фамилия</label>
                            <input type="text" name="lastName" value="${account.lastName}" class="form-last-name form-control" id="form-last-name">
                        </div>
                        <div class="form-group">
                            <label for="form-middle-name">Отчество</label>
                            <input type="text" name="middleName" value="${account.middleName}" class="form-last-name form-control" id="form-middle-name">
                        </div>

                        <div class="form-group">
                            <label for="date" class="label-date">Дата рождения</label>
                            <div class="container-date">
                                <div class='input-group date' id='datetimepicker1'>
                                    <fmt:formatDate value="${account.birthDate}" pattern="dd.MM.yyyy" var="newdatevar" />
                                    <input type='text' class="form-control" id="date" name="birthDate" value="${newdatevar}" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <%--<div class="form-group">--%>
                            <%--<label class="sr-only" for="form-email">Пол</label>--%>
                            <%--<input type="text" name="sex" value="${account.sex}" class="form-email form-control" id="form-email">--%>
                        <%--</div>--%>
                        <div class="form-group">
                            <label for="form-info">О себе</label>
                            <textarea type="text" name="info" class="form-info form-control" id="form-info">${account.info}</textarea>
                        </div>
                        <div class="form-group phones" id="phones">
                            <div class="wrap-phones">Телефоны</div>
                            <input type="hidden" value="${account.id}" id="account-id">
                            <c:forEach items="${account.phones}" var="phone" varStatus="i">
                                <div class="phone">
                                    <input type="text" name="phones[${i.index}].phone" id="phone${i.index}" value="${phone.phone}" class="form-phone form-control input-phone"
                                           placeholder="+7 (999) 999-9999" data-toggle="popover" data-placement="left"
                                           data-content="Поле телефона не может быть пустым">
                                    <button class="btn btn-danger remove" >-</button>
                                </div>
                            </c:forEach>
                            <div class="wrap-btn">
                                <button id="add-phone" class="btn add">Добавить телефон</button>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="form-skype">Skype</label>
                            <input type="text" name="skype" value="${account.skype}" class="form-skype form-control" id="form-skype">
                        </div>
                        <div class="form-group">
                            <label for="form-telegram">Telegram</label>
                            <input type="text" name="telegram" value="${account.telegram}" class="form-telegram form-control" id="form-telegram">
                        </div>
                        <div class="form-group">
                            <label for="form-home-address">Домашний адрес</label>
                            <input type="text" name="homeAddress" value="${account.homeAddress}" class="form-home-address form-control" id="form-home-address">
                        </div>
                        <div class="form-group">
                            <label for="form-work-address">Рабочий адрес</label>
                            <input type="text" name="workAddress" value="${account.workAddress}" class="form-work-address form-control" id="form-work-address">
                        </div>
                        <div class="form-group">
                            <label for="form-email">Email</label>
                            <input type="text" name="email" value="${account.email}" class="form-email form-control" id="form-email">
                        </div>

                        <div class="form-group" style="margin-top:25px">
                            <label for="uploadXML">
                                <h>Загрузить XML</h>
                                <input type="file" id="uploadXML" style="display:none">
                            </label>
                        </div>
                        <div class="form-group" style="margin-top:25px">
                            <a id="downloadXMLlink" download="account.xml" href="#">Скачать XML
                            </a>
                        </div>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <!-- Button trigger modal -->
                        <button id="modal-btn" type="button" class="btn btn-save">Сохранить</button>

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Уверены ли вы, что хотите сохранить изменения?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-answer">Да</button>
                                        <button type="button" class="btn btn-answer"><a href="/account">Нет</a></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<%@ include file="common/footer.jsp" %>