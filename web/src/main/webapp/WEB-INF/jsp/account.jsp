<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="common/header.jsp" %>
    <div class="row">
        <div class="tab-content no-border padding-24">
            <div id="home" class="tab-pane in active">
                <div class="col-xs-12 col-sm-4 center">
                        <div class="profile-picture avatar-big">
                            <c:choose>
                                <c:when test="${account.avatar != null}">
                                    <img class="editable img-responsive avatar" src="data:image/jpeg;base64,${avatar}"/>
                                </c:when>
                                <c:otherwise>
                                    <img src="${pageContext.request.contextPath}/img/avatar.png"
                                         class="editable img-responsive avatar">
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <c:if test='${relation != "MY_ACCOUNT"}'>
                            <a href="${pageContext.request.contextPath}/account/${authAccount.id}"
                               class="btn btn-sm btn-friend">
                                <span class="bigger-110">Вернуться на свою страницу</span>
                            </a>
                        </c:if>
                        <c:if test='${relation == "GUEST"}'>
                            <a href="${pageContext.request.contextPath}/sendFriendRequest?accId=${account.id}"
                               class="btn btn-sm btn-friend">
                                <i class="ace-icon fa fa-plus-circle bigger-120"></i>
                                <span class="bigger-110">Отправить запрос</span>
                            </a>
                        </c:if>
                        <c:if test='${relation == "FOLLOWER"}'>
                            <a href="${pageContext.request.contextPath}/acceptFriendRequest?accId=${account.id}"
                               class="btn btn-sm btn-friend">
                                <i class="ace-icon fa fa-plus-circle bigger-120"></i>
                                <span class="bigger-110">Добавить в друзья</span>
                            </a>
                            <a href="${pageContext.request.contextPath}/deleteFriend?accId=${account.id}"
                               class="btn btn-sm btn-friend">
                                <i class="ace-icon fa fa-plus-circle bigger-120"></i>
                                <span class="bigger-110">Удалить запрос</span>
                            </a>
                        </c:if>
                        <c:if test='${relation == "RECIPIENT"}'>
                            <a href="${pageContext.request.contextPath}/deleteFriend?accId=${account.id}"
                               class="btn btn-sm btn-friend">
                                <i class="ace-icon fa fa-plus-circle bigger-120"></i>
                                <span class="bigger-110">Удалить запрос</span>
                            </a>
                        </c:if>
                        <c:if test='${relation == "FRIEND"}'>
                            <a type="button" class="btn btn-sm btn-friend disabled">Вы друзья</a>
                            <a href="${pageContext.request.contextPath}/deleteFriend?accId=${account.id}"
                               class="btn btn-sm btn-friend">
                                <i class="ace-icon fa fa-plus-circle bigger-120"></i>
                                <span class="bigger-110">Удалить из друзей</span>
                            </a>
                        </c:if>
                        <c:if test='${relation == "MY_ACCOUNT"}'>
                            <a href="${pageContext.request.contextPath}/showRelatedAccounts?accId=${account.id}"
                               class="btn btn-sm btn-friend">
                                <i class="ace-icon fa fa-plus-circle bigger-120"></i>
                                <span class="bigger-110">Показать друзей</span>
                            </a>
                        </c:if>
                    </div>
                <div class="col-xs-12 col-sm-8">
                    <div class="user-info">
                        <h4 class="user-name">
                            <c:out value="${account.firstName}"/>
                            <c:out value="${account.middleName}"/>
                            <c:out value="${account.lastName}"/>
                        </h4>
                        <c:if test='${authAccount.id == account.id}'>
                            <a href="${pageContext.request.contextPath}/update" class="link-update">Редактировать</a>
                        </c:if>
                        <div class="profile-user-info">
                            <div class="profile-info-row">
                                <div class="profile-info-title">Дата рождения:</div>
                                <div>
                                    <i class="fa fa-birthday-cake bigger-110"></i>
                                    <fmt:formatDate value="${account.birthDate}" pattern="dd.MM.yyyy" var="newdatevar" />
                                    <c:out value="${newdatevar}" />
                                </div>
                            </div>
                            <div class="profile-info-row">
                                    <div class="profile-info-title">О себе:</div>
                                    <div>
                                        <p><c:out value="${account.info}"/></p>
                                    </div>
                                </div>
                            <div class="profile-info-row">
                                <div class="contact-info-title">Контактная информация</div>
                            </div>
                            <div class="profile-info-row">
                                <div class="profile-info-title">Телефон:</div>
                                <c:forEach items="${account.phones}" var="phone">
                                    <div class="phone"><i class="fa fa-phone bigger-110"></i>
                                        <c:out value="${phone.phone}"/>
                                    </div>
                                </c:forEach>
                            </div>
                            <div class="profile-info-row">
                                <div class="profile-info-title">Email:</div>
                                <div>
                                    <i class="fa fa-envelope-o bigger-110"></i>
                                    <c:out value="${account.email}"/>
                                </div>
                            </div>
                            <div class="profile-info-row">
                                <div class="profile-info-title">Skype:</div>
                                <div>
                                    <i class="fa fa-skype bigger-110"></i>
                                    <c:out value="${account.skype}"/>
                                </div>
                            </div>
                            <div class="profile-info-row">
                                <div class="profile-info-title">Telegram:</div>
                                <div>
                                    <i class="fa fa-paper-plane bigger-110"></i>
                                    <c:out value="${account.telegram}"/>
                                </div>
                            </div>

                            <div class="profile-info-row">
                                <div class="profile-info-title">Домашний адрес:</div>
                                <div>
                                     <i class="fa fa-map-marker bigger-110"></i>
                                    <c:out value="${account.homeAddress}"/>
                                </div>
                            </div>
                            <div class="profile-info-row">
                                <div class="profile-info-title">Рабочий адрес:</div>
                                <div><i class="fa fa-map-marker bigger-110"></i>
                                    <c:out value="${account.workAddress}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </div>
        </div>
    </div>
<%@ include file="common/footer.jsp" %>
