package com.getjavajob.training.web1706.common;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "friend_tbl")
public class Request implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "account_id_from", nullable = false)
    private Account account;

    @Id
    @ManyToOne
    @JoinColumn(name = "account_id_to", nullable = false)
    private Account friend;

    @Column(name = "is_accepted")
    private boolean accepted;


    public Request(Account account, Account friend) {
        this.account = account;
        this.friend = friend;
    }

    public Request() {
    }

    public Account getFriend() {
        return friend;
    }

    public void setFriend(Account friend) {
        this.friend = friend;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    @Override
    public String toString() {
        return "Request{" +
                " account=" + account +
                ", friend=" + friend +
                ", accepted=" + accepted +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Request request = (Request) o;

        if (accepted != request.accepted) return false;
        if (!account.equals(request.account)) return false;
        return friend.equals(request.friend);
    }

    @Override
    public int hashCode() {
        int result = account.hashCode();
        result = 31 * result + friend.hashCode();
        result = 31 * result + (accepted ? 1 : 0);
        return result;
    }
}
