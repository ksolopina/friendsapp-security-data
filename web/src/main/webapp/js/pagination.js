$(document).ready(function() {
    var navigation = $('#pagination');
    var countAccountsPerPage = $('#countAccountsPerPage').html();
    var countPages = $('#countPages').html();
    var previous = navigation.find('#previous');
    var next = navigation.find('#next');
    var list = $('#ul-pagination');
    var page = $('#pageNumber');

    $(function () {
        prepareForPagination();
        pagination();
    });

    function pagination() {
        var link = $(".pagination li a");
        
        navigation.on('click', '.page-number', function (e) {
            e.preventDefault();

            var pageNumber = $(this).html();
            link.removeClass("disabled");
            $(this).addClass("disabled");
            page.text(pageNumber);
            var start = pageNumber * countAccountsPerPage - countAccountsPerPage - 1;
            disableButton(page.text());
            ajaxFunction(start);
        });

        function ajaxFunction(start) {
            var search = $('#filter').html();
            $.ajax({
                url: ctx + '/showNextPage',
                dataType: 'json',
                data: {
                    search: search,
                    start: start,
                    maxResult: countAccountsPerPage
                },
                success: function (data) {
                    $.each(data, function (i, account) {
                        console.log(start);
                        var avatar = $('#avatar' + i);
                        var linkShow = $('#link' + i);
                        $('div#blockSearch' + i).show();
                        if (account.avatar !== null) {
                            avatar.attr("src", "data:image/jpeg;base64," + account.avatar);
                        } else {
                            avatar.attr("src", ctx + "/img/avatar.png");
                        }
                        linkShow.attr("href", ctx + "/account/" + account.id)
                            .text(account.firstName + " " + account.lastName);
                    });
                    if (data.length < countAccountsPerPage) {
                        var i = data.length;
                        while (i < countAccountsPerPage) {
                            $('div#blockSearch' + i).hide();
                            i++;
                        }
                    }
                },
            });
        }

        navigation.on('click', '#next', function (e) {
            e.preventDefault();
            page.text(parseInt(page.text()) + 1);
            link.removeClass("disabled");
            $('#link-id' + page.text()).addClass("disabled");
            var start = (parseInt(page.text())) * countAccountsPerPage - countAccountsPerPage;
            disableButton(page.text());
            ajaxFunction(start);
        });

        navigation.on('click', '#previous', function (e) {
            e.preventDefault();
            page.text(parseInt(page.text()) - 1);
            link.removeClass("disabled");
            $('#link-id' + page.text()).addClass("disabled");
            var start = (parseInt(page.text()) - 1) * countAccountsPerPage - countAccountsPerPage;
            disableButton(page.text());
            ajaxFunction(start);
        });
    }

    function disableButton(currentPage) {
        var linkPrevious = $("#previous a");
        var linkNext = $("#next a");

        if (parseInt(currentPage) === 1) {
            linkPrevious.addClass("disabled");
        } else {
            linkPrevious.removeClass("disabled");
        }
        if (parseInt(currentPage) === parseInt(countPages)) {
            linkNext.addClass("disabled");
        } else {
            linkNext.removeClass("disabled");
        }
    }
    
    function prepareForPagination() {
        if (countPages > 1) {
            list.append('<li class="page-item" id="previous">' +
                    '<a class="page-link" href="#" aria-label="Previous">' +
                    '<span aria-hidden="true">&laquo;</span>' +
                    '<span class="sr-only">Previous</span>' +
                    '</a>' +
                    '</li>');

            for (var i = 1; i <= countPages; i++) {
                list.append('<li class="page-item">' +
                    '<a class="page-link page-number" href="#" id="link-id' + i +'">'
                    + i +
                    '</a>' +
                    '</li>')
            }

            list.append('<li class="page-item" id="next"> ' +
                '<a class="page-link next" href="#" aria-label="Next">' +
                '<span aria-hidden="true">&raquo;</span>' +
                '<span class="sr-only">Next</span>' +
                '</a>' +
                '</li>');
        $("#previous a").addClass("disabled");    
        $('#link-id1').addClass("disabled");     
        }
    }

});