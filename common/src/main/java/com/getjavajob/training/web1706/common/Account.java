package com.getjavajob.training.web1706.common;

import org.hibernate.annotations.DynamicUpdate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;

import static javax.persistence.CascadeType.MERGE;

@Entity
@Table(name = "account_tbl", uniqueConstraints = {@UniqueConstraint(columnNames = "email")})
@DynamicUpdate
public class Account  implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "account_id", unique = true, nullable = false)
    private int id;
    @Column(name = "last_name", nullable = false)
    private String lastName;
    @Column(name = "first_name", nullable = false)
    private String firstName;
    @Column(name = "middle_name")
    private String middleName;
    @Column(name = "birth_date")
    private Date birthDate;
    @Column(name = "email", nullable = false, unique = true)
    private String email;
    @Column(name = "password", nullable = false)
    private String password;
    @Column(name = "sex")
    private char sex;
    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private List<Phone> phones;
    @Column(name = "address_home")
    private String homeAddress;
    @Column(name = "address_work")
    private String workAddress;
    @Column(name = "telegram")
    private String telegram;
    @Column(name = "additional_information")
    private String info;
    @OneToMany(mappedBy = "friend", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private List<Request> requestsTo;
    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private List<Request> requestsFrom;
    @Lob
    @Column(name = "pic", length = 100000)
    private byte[] avatar;
    @Column(name = "skype")
    private String skype;
    @ManyToMany
    @JoinTable(name = "account_role", joinColumns = @JoinColumn(name = "account_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;

    public Account() {
        requestsFrom = new ArrayList<>();
        requestsTo = new ArrayList<>();
        phones = new ArrayList<>();
        roles = new HashSet<>();
    }

    public Account(String lastName, String firstName, String middleName,
                   Date birthDate, String email, String password, char sex,
                   String homeAddress, String workAddress, String telegram, String info) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.birthDate = birthDate;
        this.email = email;
        this.password = password;
        this.sex = sex;
        this.homeAddress = homeAddress;
        this.workAddress = workAddress;
        this.telegram = telegram;
        this.info = info;
        requestsFrom = new ArrayList<>();
        requestsTo = new ArrayList<>();
        phones = new ArrayList<>();
        roles = new HashSet<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public void addPhone(Phone phone) {
        this.phones.add(phone);
        phone.setAccount(this);
    }

    public void removePhone(Phone phone) {
        this.phones.remove(phone);
    }

    public void addPhones(List<Phone> phones) {
        this.phones.addAll(phones);
    }

    public void removePhones() {
        this.phones.clear();
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getTelegram() {
        return telegram;
    }

    public void setTelegram(String telegram) {
        this.telegram = telegram;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void addRequestsFromMe(Account friend) {
        Request newRequest = new Request(this, friend);
        this.requestsFrom.add(newRequest);
        friend.requestsTo.add(newRequest);
    }

    public void removeRequestsFromMe(Account friend) {
        Request newRequest = new Request(this, friend);
        this.requestsFrom.remove(newRequest);
        friend.requestsTo.remove(newRequest);
    }

    public void addRequestsToMe(Account friend) {
        Request newRequest = new Request(friend, this);
        this.requestsTo.add(newRequest);
        friend.requestsFrom.add(newRequest);
    }

    public void removeRequestsToMe(Account friend) {
        Request newRequest = new Request(friend, this);
        this.requestsTo.remove(newRequest);
        friend.requestsFrom.remove(newRequest);
    }

    public List<Request> getRequestsTo() {
        return requestsTo;
    }

    public void setRequestsTo(List<Request> requestsTo) {
        this.requestsTo = requestsTo;
    }

    public List<Request> getRequestsFrom() {
        return requestsFrom;
    }

    public void setRequestsFrom(List<Request> requestsFrom) {
        this.requestsFrom = requestsFrom;
    }


    public void setPassword(String password) {
        this.password = password;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> authorities = new HashSet<>();
        for (Role role: getRoles()) {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        }
        return authorities;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        return email.equals(account.email);
    }

    @Override
    public int hashCode() {
        return email.hashCode();
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", birthDate=" + birthDate +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", sex=" + sex +
                ", phones=" + phones +
                ", homeAddress='" + homeAddress + '\'' +
                ", workAddress='" + workAddress + '\'' +
                ", telegram='" + telegram + '\'' +
                ", info='" + info + '\'' +
                ", skype='" + skype + '\'' +
                '}';
    }
}
