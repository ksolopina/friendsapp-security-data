<%@ include file="common/header.jsp" %>
<div class="search-wrapper">
    <c:set var="index" value="0"/>
    <c:forEach items="${searchAccounts}" var="account">
        <div class="search" id="blockSearch${index}">
            <div class="profile-picture avatar-small">
                <c:choose>
                    <c:when test="${account.avatar != null}">
                        <img id="avatar${index}" class="editable img-responsive avatar" src="data:image/jpeg;base64,${account.avatar}"/>
                    </c:when>
                    <c:otherwise>
                        <img id="avatar${index}" src="${pageContext.request.contextPath}/img/avatar.png"
                             class="editable img-responsive avatar">
                    </c:otherwise>
                </c:choose>
            </div>
            <div>
                <a href="${pageContext.request.contextPath}/account/${account.id}" id="link${index}">${account.firstName} ${account.lastName}</a>
            </div>
        </div>
        <c:set var="index" value="${index + 1}"/>
    </c:forEach>

    <div class="hidden" id="countAccountsPerPage">${countAccountsPerPage}</div>
    <div class="hidden" id="countPages">${countPages}</div>
    <div class="hidden" id="filter">${search}</div>
    <div class="hidden" id="pageNumber">1</div>
</div>
<nav aria-label="Page navigation" id="pagination">
    <ul class="pagination" id="ul-pagination">
    </ul>
</nav>
<%@ include file="common/footer.jsp" %>