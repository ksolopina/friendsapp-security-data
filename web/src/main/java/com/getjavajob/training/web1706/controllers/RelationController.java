package com.getjavajob.training.web1706.controllers;

import com.getjavajob.training.web1706.common.Account;
import com.getjavajob.training.web1706.common.DTO.AccountDTO;
import com.getjavajob.training.web1706.service.GenericService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import javax.servlet.ServletException;
import java.util.List;

import static com.getjavajob.training.web1706.utils.SecurityUtils.getAuthenticatedAccount;
import static com.getjavajob.training.web1706.utils.Utils.getListAccountDTO;

@Controller
public class RelationController {
    private static final Logger logger = LoggerFactory.getLogger(RelationController.class);

    @Autowired
    private GenericService service;

    @RequestMapping(value = "/sendFriendRequest")
    public ModelAndView sendFriendRequest(@RequestParam("accId") int accId) {
        Account authAccount = getAuthenticatedAccount();
        Account account = service.getAccount(authAccount.getId());
        Account friend = service.getAccount(accId);
        service.sendRequestToFriend(account, friend);
        logger.info("request from account " + authAccount.getId() + " to account "  + accId);
        logger.info("account with request" + service.getAccountRelation(account.getId(), friend.getId()));
        return new ModelAndView("redirect:/account/" + accId);
    }

    @RequestMapping(value = "/acceptFriendRequest")
    public ModelAndView acceptFriendRequest(@RequestParam("accId") int accId) {
        Account authAccount = getAuthenticatedAccount();
        Account account = service.getAccount(authAccount.getId());
        Account friend = service.getAccount(accId);
        service.acceptFriend(account, friend);
        service.getAccountRelation(account.getId(), friend.getId());
        logger.info("account " + authAccount.getId() + " accepted account " + accId);
        return new ModelAndView("redirect:/account/" + accId);
    }

    @RequestMapping(value = "/deleteFriend")
    public ModelAndView deleteFriend(@RequestParam("accId") int accId) {
        Account authAccount = getAuthenticatedAccount();
        Account account = service.getAccount(authAccount.getId());
        Account friend = service.getAccount(accId);
        service.deleteRequest(account, friend);
        logger.info("account " + authAccount.getId() + " deleted account " + accId);
        logger.info("account " + service.getAccountRelation(account.getId(), friend.getId()));
        return new ModelAndView("redirect:/account/" + accId);
    }

    @RequestMapping(value = "/showRelatedAccounts")
    public String handleRequest(@RequestParam("accId") int accId, RedirectAttributes ra) {
        Account account = service.getAccount(accId);
        ra.addAttribute("accId", accId);
        ra.addAttribute("countFriends", service.getCountFriends(account));
        ra.addAttribute("countFollowers", service.getCountFollowers(account));
        ra.addAttribute("countRecipients",  service.getCountRecipients(account));
        return "redirect:/showFriends";
    }

    @RequestMapping(value = "/showFriends")
    public ModelAndView showFriends(@RequestParam("accId") int accId, @RequestParam("countFriends") int countFriends,
                                    @RequestParam("countFollowers") int countFollowers,
                                    @RequestParam("countRecipients") int countRecipients) throws ServletException {
        Account account = service.getAccount(accId);
        List<Account> accounts = service.getFriends(account);
        logger.info("show friends ");
        return showAccounts(accounts, account, countFriends, countFollowers, countRecipients);
    }

    @RequestMapping(value = "/showFollowers")
    public ModelAndView showFollowers(@RequestParam("accId") int accId, @RequestParam("countFriends") int countFriends,
                                      @RequestParam("countFollowers") int countFollowers,
                                      @RequestParam("countRecipients") int countRecipients) throws ServletException {
        Account account = service.getAccount(accId);
        List<Account> accounts = service.getFollowers(account);
        return showAccounts(accounts, account, countFriends, countFollowers, countRecipients);
    }

    @RequestMapping(value = "/showRecipients")
    public ModelAndView showRecipients(@RequestParam("accId") int accId, @RequestParam("countFriends") int countFriends,
                                       @RequestParam("countFollowers") int countFollowers,
                                       @RequestParam("countRecipients") int countRecipients) throws ServletException {
        Account account = service.getAccount(accId);
        List<Account> accounts = service.getRecipients(account);
        return showAccounts(accounts, account, countFriends, countFollowers, countRecipients);
    }

    private ModelAndView showAccounts(List<Account> accounts, Account account, int countFriends,
                                       int countFollowers, int countRecipients) throws ServletException {
        ModelAndView modelAndView = new ModelAndView("/friends");
        List<AccountDTO> allAccounts = getListAccountDTO(accounts);
        modelAndView.addObject("accounts", allAccounts);
        modelAndView.addObject("account", account);
        modelAndView.addObject("countFriends", countFriends);
        modelAndView.addObject("countFollowers", countFollowers);
        modelAndView.addObject("countRecipients", countRecipients);
        return modelAndView;
    }

}
