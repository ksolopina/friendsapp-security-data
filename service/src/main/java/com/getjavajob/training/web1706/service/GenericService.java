package com.getjavajob.training.web1706.service;

import com.getjavajob.training.web1706.common.*;

import java.util.List;

public interface GenericService {

    Account getAccount(int id);

    List<Account> getAllAccounts();

    Account getByEmail(String email);

    byte[] getAvatarById(int id);

    Account createAccount(Account account);

    Account updateAccount(Account account);

    void deleteAccount(Account account);

    void sendRequestToFriend(Account account, Account friend);

    void deleteRequest(Account account, Account friend);

    void acceptFriend(Account account, Account friend);

    List<Account> getFriends(Account account);

    int getCountFriends(Account account);

    List<Account> getFollowers(Account account);

    int getCountFollowers(Account account);

    List<Account> getRecipients(Account account);

    int getCountRecipients(Account account);

    List<Phone> getPhones(Account account);

    List<Account> searchAccounts(String expr);

    List<Account> searchAccountsForPagination(String expr, int start, int maxResult);

    int getCountAccountsForPagination(String expr);

    Relation getAccountRelation(int idFrom, int idTo);
}
