package com.getjavajob.training.web1706.controllers;

import com.getjavajob.training.web1706.common.Account;
import com.getjavajob.training.web1706.common.Phone;
import com.getjavajob.training.web1706.common.Relation;
import com.getjavajob.training.web1706.service.GenericService;
import com.getjavajob.training.web1706.service.SecurityService;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.getjavajob.training.web1706.utils.SecurityUtils.getAuthenticatedAccount;
import static com.getjavajob.training.web1706.utils.Utils.getEncodedFile;

@Controller
public class AccountController {
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private GenericService service;

    @Autowired
    private SecurityService securityService;

    @InitBinder
    protected void initBinder(ServletRequestDataBinder binder) {
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }

    @RequestMapping(value = "/account/{accId}", method = RequestMethod.GET)
    public ModelAndView getAccountPage(@PathVariable int accId) throws ServletException {
        logger.info("page accountId = " + accId);
        ModelAndView modelAndView = new ModelAndView("account");
        Account authAccount = getAuthenticatedAccount();
        modelAndView.addObject("authAccount", authAccount);
        Account account = service.getAccount(accId);
        Relation relation =  service.getAccountRelation(authAccount.getId(), accId);
        modelAndView.addObject("relation", relation.name());
        logger.info("relation " + relation.name());
        modelAndView.addObject("account", account);
        String encodedFile = getEncodedFile(service.getAvatarById(accId));
        if (encodedFile != null) {
            modelAndView.addObject("avatar", encodedFile);
            logger.info("avatar was added");
        }
        return modelAndView;
    }

    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public ModelAndView updatePage() {
        Account authAccount = getAuthenticatedAccount();
        logger.info("update page for accountId = " + authAccount.getId());
        authAccount = service.getAccount(authAccount.getId());
        ModelAndView modelAndView = new ModelAndView("update");
        modelAndView.addObject("account", authAccount);
        return modelAndView;
    }

    @RequestMapping(value = "/doUpdate", method = RequestMethod.POST, headers = {"content-type=multipart/form-data"})
    public String doUpdate(@ModelAttribute("account") Account account) throws ServletException, IOException {
        Account authAccount = getAuthenticatedAccount();

        account.setId(authAccount.getId());
        account.setPassword(authAccount.getPassword());

        byte[] avatar = account.getAvatar();
        if (avatar == null || avatar.length == 0) {
            account.setAvatar(service.getAvatarById(authAccount.getId()));
        }

        removeNullPhones(account);
        preparePhone(account);

        service.updateAccount(account);

        logger.info("succes update for " + account.getId());
        return "redirect:/account/" + account.getId();
    }

    private void preparePhone(Account account) {
        List<Phone> newPhones = account.getPhones();
        if (newPhones != null) {
            for (Phone phone : newPhones) {
                phone.setAccount(account);
            }
        } else {
            account.setPhones(service.getPhones(account));
            account.getPhones().clear();
        }
    }

    private Account removeNullPhones(Account account) {
        if (account.getPhones() != null) {
           account.getPhones().removeIf(phone -> phone.getPhone() == null);
        }
        return account;
    }

}