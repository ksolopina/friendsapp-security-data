package com.getjavajob.training.web1706.controllers;

import com.getjavajob.training.web1706.common.Account;
import com.getjavajob.training.web1706.common.Role;
import com.getjavajob.training.web1706.service.GenericService;
import com.getjavajob.training.web1706.service.SecurityService;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

import static com.getjavajob.training.web1706.utils.SecurityUtils.getAuthenticatedAccount;

@Controller
public class MainController {
    private static final Logger logger = LoggerFactory.getLogger(MainController.class);

    @Autowired
    private GenericService service;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private SecurityService securityService;

    @RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    public String getWelcomePage() {
        Account authenticatedAccount = getAuthenticatedAccount();
        if (authenticatedAccount != null) {
            return "redirect:/account/" + authenticatedAccount.getId();
        }
        return "/welcome";
    }

    @RequestMapping(value = "/account")
    public String account() {
        return "redirect:/account/" + getAuthenticatedAccount().getId();
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ModelAndView registration(@ModelAttribute("account") Account account, HttpServletRequest req) {
        account.setPassword(bCryptPasswordEncoder.encode(account.getPassword()));
        service.createAccount(account);
        securityService.authenticate(account.getEmail());
        logger.info("succes registration");
        return new ModelAndView("redirect:/account/"+ account.getId());

    }
}
