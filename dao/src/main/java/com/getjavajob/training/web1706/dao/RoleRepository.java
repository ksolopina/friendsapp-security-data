package com.getjavajob.training.web1706.dao;

import com.getjavajob.training.web1706.common.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {

    Role findByName(String name);

}
