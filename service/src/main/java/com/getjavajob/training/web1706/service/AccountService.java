package com.getjavajob.training.web1706.service;

import com.getjavajob.training.web1706.common.*;
import com.getjavajob.training.web1706.dao.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service("accountService")
public class AccountService implements GenericService {
    private static final Logger logger = LoggerFactory.getLogger(AccountService.class);

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private RoleRepository roleRepository;

    public AccountService() {
    }

    public AccountService(AccountRepository accountRepository, RoleRepository roleRepository) {
        this.accountRepository = accountRepository;
        this.roleRepository = roleRepository;
    }

    public Account getAccount(int id) {
        return accountRepository.findOne(id);
    }

    public List<Account> getAllAccounts() {
        return accountRepository.findAll();
    }

    public Account getByEmail(String email) {
        Account account = new Account();
        if (email != null) {
            account = accountRepository.findByEmail(email);
            if (account != null) {
                List<Phone> phones = account.getPhones();
                if (phones != null) {
                    phones.size();
                }
            }
        }
        return account;
    }

    public byte[] getAvatarById(int id) {
        return accountRepository.findOne(id).getAvatar();
    }

    @Transactional
    public Account createAccount(Account account) {
        if (account != null) {
            Set<Role> roles = new HashSet<>();
            roles.add(roleRepository.findByName("ROLE_USER"));
            account.setRoles(roles);
            return accountRepository.save(account);
        }
        return null;
    }

    @Transactional
    public Account updateAccount(Account account) {
        logger.info("Started method");
        logger.info("account id "+account.getId());
        if (account != null) {
            Account accountDB = accountRepository.findOne(account.getId());
            logger.info("account from DB " + accountDB);
            logger.info("read lazy RequestsFrom collections " + account.getRequestsFrom());
            account.setRequestsFrom(accountDB.getRequestsFrom());
            logger.info("read lazy RequestsTo collections " + account.getRequestsTo());
            account.setRequestsTo(accountDB.getRequestsTo());
            logger.info("Finished method");
            return accountRepository.save(account);
        }
        return null;
    }

    @Transactional
    public void deleteAccount(Account account) {
        if (account != null) {
            accountRepository.delete(account);
        }
    }

    @Transactional
    public void sendRequestToFriend(Account account, Account friend) {
        logger.info("Started method");
        if (account != null && friend != null) {
            Account accountFrom = accountRepository.findOne(account.getId());
            Account accountTo = accountRepository.findOne(friend.getId());
            logger.info("account "+ accountFrom);
            logger.info("account "+ accountTo);
            accountFrom.addRequestsFromMe(accountTo);
        }
    }

    @Transactional
    public void deleteRequest(Account account, Account friend) {
        if (friend != null) {
            Account fromAccount = accountRepository.findOne(account.getId());
            Account toAccount =  accountRepository.findOne(friend.getId());
            rejectFriend(fromAccount, toAccount);
            fromAccount.removeRequestsFromMe(toAccount);
            fromAccount.removeRequestsToMe(toAccount);
        }
    }

    @Transactional
    public void acceptFriend(Account account, Account friend) {
        logger.info("Started method");
        if (account != null && friend != null) {
            Account fromAccount = accountRepository.findOne(account.getId());
            Account toAccount = accountRepository.findOne(friend.getId());
            logger.info("account request " + fromAccount.getRequestsTo());
            logger.info("friend request " + toAccount.getRequestsFrom());
            for (Request req : fromAccount.getRequestsTo()) {
                if (req.getAccount().equals(toAccount) && req.getFriend().equals(fromAccount)) {
                    req.setAccepted(true);
                }
            }
            logger.info("account request " + fromAccount.getRequestsTo());
            logger.info("friend request " + toAccount.getRequestsFrom());
        }
    }

    @Transactional
    public void rejectFriend(Account account, Account friend) {
        if (account != null && friend != null) {
            Account fromAccount = accountRepository.findOne(account.getId());
            Account toAccount = accountRepository.findOne(friend.getId());
            for (Request req : fromAccount.getRequestsTo()) {
                if (req.getAccount().equals(toAccount) && req.getFriend().equals(fromAccount)) {
                    req.setAccepted(false);
                }
            }
            for (Request req : fromAccount.getRequestsFrom()) {
                if (req.getAccount().equals(fromAccount) && req.getFriend().equals(toAccount)) {
                    req.setAccepted(false);
                }
            }
        }
    }

    public List<Account> getFriends(Account account) {
        List<Account> friends = new ArrayList<>();
        if (account != null) {
            List<Request> requestsFrom = getAccount(account.getId()).getRequestsFrom();
            if (requestsFrom != null) {
                for (Request request : requestsFrom) {
                    if (request.getAccount().equals(account) && request.isAccepted()) {
                        friends.add(request.getFriend());
                    }
                }
            }
            List<Request> requestsTo = getAccount(account.getId()).getRequestsTo();
            if (requestsTo != null) {
                for (Request request : requestsTo) {
                    if (request.getFriend().equals(account) && request.isAccepted()) {
                        friends.add(request.getAccount());
                    }
                }
            }
        }
        return friends;
    }

    public int getCountFriends(Account account) {
        return getFriends(account).size();
    }

    public List<Account> getFollowers(Account account) {
        List<Account> followers = new ArrayList<>();
        if (account != null) {
            List<Request> requests = getAccount(account.getId()).getRequestsTo();
            if (requests != null) {
                for (Request request : requests) {
                    if (!request.isAccepted()) {
                        followers.add(request.getAccount());
                    }
                }
            }
        }
        return followers;
    }

    public int getCountFollowers(Account account) {
        return getFollowers(account).size();
    }

    public List<Account> getRecipients(Account account) {
        List<Account> recipients = new ArrayList<>();
        if (account != null) {
            List<Request> requests = getAccount(account.getId()).getRequestsFrom();
            if (requests != null) {
                for (Request request : requests) {
                    if (!request.isAccepted()) {
                        recipients.add(request.getFriend());
                    }
                }
            }
        }
        return recipients;
    }

    public int getCountRecipients(Account account) {
        return getRecipients(account).size();
    }

    public List<Phone> getPhones(Account account) {
        List<Phone> phones = new ArrayList<>();
        if (account != null) {
            phones = getAccount(account.getId()).getPhones();
        }
        return phones;
    }

    public List<Account> searchAccounts(String expr) {
        return accountRepository.findAllAccountsByPattern(expr, new PageRequest(0, Integer.MAX_VALUE));
    }

    public List<Account> searchAccountsForPagination(String expr, int start, int maxResult) {
        return accountRepository.findAllAccountsByPattern(expr, new PageRequest(start, maxResult));
    }

    public int getCountAccountsForPagination(String expr) {
        return accountRepository.countAccountsByPattern(expr);
    }

    public Relation getAccountRelation(int idFrom, int idTo) {
        if (idFrom == idTo) {
            return Relation.MY_ACCOUNT;
        }

        Account from = accountRepository.findOne(idFrom);
        Account to = accountRepository.findOne(idTo);

        boolean isFriend = getFriends(from).contains(to);
        if (isFriend) {
            return Relation.FRIEND;
        }
        boolean isFollower = getFollowers(from).contains(to);
        if (isFollower) {
            return Relation.FOLLOWER;
        }
        boolean isRecipient = getRecipients(from).contains(to);
        if (isRecipient) {
            return Relation.RECIPIENT;
        }

        return Relation.GUEST;
    }
}
