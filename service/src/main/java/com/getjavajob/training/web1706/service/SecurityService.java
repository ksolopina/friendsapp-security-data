package com.getjavajob.training.web1706.service;

public interface SecurityService {

    void authenticate(String username);
}
