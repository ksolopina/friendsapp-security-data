<%@ include file="common/headerWelcome.jsp" %>
<c:if test = "${account != null}">
    <c:redirect url="/account/${account.id}"/>
</c:if>
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-sm-5">
                <div class="form-box">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>Войдите на сайт</h3>
                            <p>Введите логин и пароль:</p>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-key"></i>
                        </div>
                    </div>
                    <div class="form-bottom">
                        <p class="alert-message">${message}</p>
                        <form role="form" action="/login" method="post" class="login-form">
                            <div class="form-group">
                                <label class="sr-only" for="login-form-username">Email</label>
                                <input type="text" name="email" placeholder="email" class="login-form-username form-control" id="login-form-username">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="login-form-password">Password</label>
                                <input type="password" name="password" placeholder="Пароль" class="login-form-password form-control" id="login-form-password">
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" id="remember"> Запомнить меня
                                </label>
                            </div>
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            <button type="submit" class="btn">Войти!</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-1 middle-border"></div>
            <div class="col-sm-1"></div>
            <div class="col-sm-5">
                <div class="form-box">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>Зерегистрируйтесь сейчас</h3>
                            <p>Заполните форму:</p>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-pencil"></i>
                        </div>
                    </div>
                    <div class="form-bottom">
                        <p class="alert-message">
                            <c:out value="${messageRegistration}"/>
                        </p>
                        <form role="form" action="/registration" method="post" class="registration-form">
                            <div class="form-group">
                                <label class="sr-only" for="form-first-name">First name</label>
                                <input type="text" name="firstName" placeholder="Имя" class="form-first-name form-control" id="form-first-name">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-last-name">Last name</label>
                                <input type="text" name="lastName" placeholder="Фамилия" class="form-last-name form-control" id="form-last-name">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-email">Email</label>
                                <input type="text" name="email" placeholder="Email" class="form-email form-control" id="form-email">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-password">Password</label>
                                <input type="password" name="password" placeholder="Пароль..." class="form-password form-control" id="form-password">
                            </div>
                            <%--<div class="form-group">--%>
                                <%--<label class="sr-only" for="form-about-yourself">About yourself</label>--%>
                                <%--<textarea name="form-about-yourself" placeholder="Немного о себе..."--%>
                                          <%--class="form-about-yourself form-control" id="form-about-yourself"></textarea>--%>
                            <%--</div>--%>
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            <button type="submit" class="btn">Зарегистрироваться!</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="common/footer.jsp" %>
