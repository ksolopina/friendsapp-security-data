package com.getjavajob.training.web1706.dao;

import com.getjavajob.training.web1706.common.Account;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;

public interface AccountRepository extends JpaRepository<Account, Integer> {

    Account findByEmail(String email);

    @Query("SELECT a FROM Account a WHERE CONCAT(a.lastName, ' ', a.firstName) LIKE CONCAT('%', :expr, '%') OR " +
            "CONCAT(a.firstName, ' ', a.lastName) LIKE CONCAT('%', :expr, '%')")
    List<Account> findAllAccountsByPattern(@Param("expr") String expr, Pageable pageable);

    @Query("SELECT COUNT(a) FROM Account a WHERE CONCAT(a.lastName, ' ', a.firstName) LIKE CONCAT('%', :expr, '%') OR " +
            "CONCAT(a.firstName, ' ', a.lastName) LIKE CONCAT('%', :expr, '%')")
    int countAccountsByPattern(@Param("expr") String expr);

}
