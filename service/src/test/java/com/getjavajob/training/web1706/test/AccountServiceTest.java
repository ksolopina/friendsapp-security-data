package com.getjavajob.training.web1706.test;

import com.getjavajob.training.web1706.common.Account;
import com.getjavajob.training.web1706.common.Phone;
import com.getjavajob.training.web1706.common.Request;
import com.getjavajob.training.web1706.common.Role;
import com.getjavajob.training.web1706.dao.AccountRepository;
import com.getjavajob.training.web1706.dao.RoleRepository;
import com.getjavajob.training.web1706.service.AccountService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageRequest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {

    private AccountRepository accountRepository;
    private RoleRepository roleRepository;
    private Account account;
    private Account friend;
    private AccountService accountService;

    @Before
    public void init() throws ParseException {
        account = new Account("Ivanov", "Ivan", "Ivanovich",
                new SimpleDateFormat("dd.MM.yyyy").parse("11.11.2011"), "ivanov@mail.ru", "2344455", 'm',
                "home address", "work address",
                "@telegram","about me");
        friend = new Account( "Petrov", "Ivan", "Ivanovich",
                new SimpleDateFormat("dd.MM.yyyy").parse("11.11.2011"), "petrov@mail.ru", "2344455", 'm',
                "home address", "work address",
                "@telegram","about me");

        accountRepository = mock(AccountRepository.class);
        roleRepository = mock(RoleRepository.class);
        account.setId(1);
        friend.setId(2);
        accountService = new AccountService(accountRepository, roleRepository);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAccountTest() {
        Account mock = mock(Account.class);
        when(accountRepository.findOne(mock.getId())).thenReturn(mock);
        assertEquals(mock, accountService.getAccount(new Account().getId()));
        assertNotEquals(mock(Account.class), accountService.getAccount(new Account().getId()));
        verify(accountRepository, times(2)).findOne(new Account().getId());
    }

    @Test
    public void getAllAccountsTest() {
        List<Account> list = new ArrayList<>();
        list.add(mock(Account.class));
        list.add(mock(Account.class));
        when(accountRepository.findAll()).thenReturn(list);
        assertEquals(list, accountService.getAllAccounts());
        verify(accountRepository, times(1)).findAll();
    }

    @Test
    public void createAccountTest() {
        when(accountRepository.save(account)).thenReturn(account);
        assertEquals(accountService.createAccount(account), account);
    }

    @Test
    public void removeAccountTest() {
        Account mock = mock(Account.class);
        accountService.deleteAccount(mock);
        verify(accountRepository).delete(mock);
    }


    @Test
    public void updateAccountTest() {
        when(accountRepository.findOne(account.getId())).thenReturn(account);
        account.setEmail("123@mail.ru");
        accountService.updateAccount(account);
        verify(accountRepository).save(account);
    }

    @Test
    public void sendRequestToFriendTest() throws ParseException {
//        Account mockAcc = mock(Account.class);
//        Account mockFriend = mock(Account.class);
        when(accountRepository.findOne(account.getId())).thenReturn(account);
        when(accountRepository.findOne(friend.getId())).thenReturn(friend);

        accountService.sendRequestToFriend(account, friend);
        assertEquals(1, accountRepository.findOne(account.getId()).getRequestsFrom().size());
        //verify(mockAcc).addRequestsFromMe(mockFriend);

    }

    @Test
    public void deleteRequestTest() throws ParseException {
        when(accountRepository.findOne(account.getId())).thenReturn(account);
        when(accountRepository.findOne(friend.getId())).thenReturn(friend);

        accountService.sendRequestToFriend(account, friend);
        accountService.acceptFriend(friend, account);
        accountService.deleteRequest(account, friend);
        assertTrue(accountRepository.findOne(account.getId()).getRequestsFrom().isEmpty());
    }

    @Test
    public void acceptFriendTest() {
        when(accountRepository.findOne(account.getId())).thenReturn(account);
        when(accountRepository.findOne(friend.getId())).thenReturn(friend);

        accountService.sendRequestToFriend(friend, account);
        assertEquals(1, accountRepository.findOne(account.getId()).getRequestsTo().size());
        assertFalse(accountRepository.findOne(account.getId()).getRequestsTo().get(0).isAccepted());
        accountService.acceptFriend(account, friend);
        assertTrue(accountRepository.findOne(account.getId()).getRequestsTo().get(0).isAccepted());
    }

    @Test
    public void rejectFriendTest() {
        when(accountRepository.findOne(account.getId())).thenReturn(account);
        when(accountRepository.findOne(friend.getId())).thenReturn(friend);
        accountService.sendRequestToFriend(friend, account);

        accountService.acceptFriend(account, friend);
        assertTrue(accountRepository.findOne(account.getId()).getRequestsTo().get(0).isAccepted());

        accountService.rejectFriend(account, friend);
        assertFalse(accountRepository.findOne(account.getId()).getRequestsTo().get(0).isAccepted());
    }

    @Test
    public void getAllFriendsTest() {

        Request request = new Request(account, friend);
        List<Request> requests = new ArrayList<>();
        requests.add(request);
        List<Account> friends = new ArrayList<>();
        friends.add(friend);

        when(accountRepository.findOne(account.getId())).thenReturn(account);
        when(accountRepository.findOne(friend.getId())).thenReturn(friend);
        accountService.sendRequestToFriend(account, friend);
        accountService.acceptFriend(friend, account);
        assertEquals(friends, accountService.getFriends(account));
    }

    @Test
    public void getAllFollowersTest() {
        Request request = new Request(friend, account);
        List<Request> requests = new ArrayList<>();
        requests.add(request);
        List<Account> followers = new ArrayList<>();
        followers.add(friend);

        when(accountRepository.findOne(account.getId())).thenReturn(account);
        when(accountRepository.findOne(friend.getId())).thenReturn(friend);
        accountService.sendRequestToFriend(friend, account);

        assertEquals(followers, accountService.getFollowers(account));
    }

    @Test
    public void getRecipientsTest() {
        Request request = new Request(friend, account);
        List<Request> requests = new ArrayList<>();
        requests.add(request);
        List<Account> recipients = new ArrayList<>();
        recipients.add(friend);

        when(accountRepository.findOne(account.getId())).thenReturn(account);
        when(accountRepository.findOne(friend.getId())).thenReturn(friend);
        accountService.sendRequestToFriend(account, friend);

        assertEquals(recipients, accountService.getRecipients(account));
    }

    @Test
    public void getAllPhonesTest() {
        Phone phone = new Phone("99999", account);
        List<Phone> phones = new ArrayList<>();
        phones.add(phone);
        account.addPhone(phone);
        when(accountRepository.findOne(account.getId())).thenReturn(account);
        assertEquals(phones, accountService.getPhones(account));
    }

    @Test
    public void searchAccountsForPaginationTest() {
        String expr = "ov";
        int start = 2;
        int maxResult = 3;
        List<Account> list = new ArrayList<>();
        when(accountRepository.findAllAccountsByPattern(expr, new PageRequest(start, maxResult))).thenReturn(list);

        List<Account> result = accountService.searchAccountsForPagination(expr, start, maxResult);
        verify(accountRepository).findAllAccountsByPattern(expr, new PageRequest(start, maxResult));
        assertEquals(list, result);
    }

    @Test
    public void getCountAccountsForPaginationTest() {
        String expr = "ov";
        int count = 2;
        when(accountRepository.countAccountsByPattern(expr)).thenReturn(count);

        int result = accountService.getCountAccountsForPagination(expr);
        verify(accountRepository).countAccountsByPattern(expr);
        assertEquals(count, result);
    }
}
