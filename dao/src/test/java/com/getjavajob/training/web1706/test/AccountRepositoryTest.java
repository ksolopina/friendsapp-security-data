package com.getjavajob.training.web1706.test;

import com.getjavajob.training.web1706.common.Account;
import com.getjavajob.training.web1706.common.Phone;
import com.getjavajob.training.web1706.dao.AccountRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.IOException;
import java.math.BigInteger;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:dao-context.xml", "classpath:dao-context-override.xml"})
public class AccountRepositoryTest {

    @Autowired
    private DataSource dataSource;
    @Autowired
    private AccountRepository accountRepository;
    private Account account;
    private List<Account> accounts;
    private Account friend;
    private Phone phone;
    private List<Phone> phones;
    private ResourceDatabasePopulator databasePopulator;

    @Before
    public void init() throws ParseException, SQLException, IOException, ClassNotFoundException {
        Resource createTables = new ClassPathResource("queriesCreate.sql");
        databasePopulator = new ResourceDatabasePopulator(createTables);
        databasePopulator.execute(dataSource);

        account = new Account("Ivanov", "Ivan", "Ivanovich",
                new SimpleDateFormat("dd.MM.yyyy").parse("11.11.2011"), "ivanov@mail.ru",
                "12345", 'm',"home address", "work address",
                "@telegram","about me");
        friend = new Account("Petrov", "Petr", "Ivanovich",
                new SimpleDateFormat("dd.MM.yyyy").parse("10.05.1990"), "petrov@mail.ru", "12344",
                'm',"home address", "work address",
                "@petro","about me");
        phone = new Phone("90100011122", account);
        phones = new ArrayList<>();
        phones.add(phone);
        account.setPhones(phones);
        accounts = new ArrayList<>();
        account = accountRepository.save(account);
        friend = accountRepository.save(friend);
        accounts.add(account);
        accounts.add(friend);
    }

    @After
    public void terminate() throws SQLException {
        Resource dropTables = new ClassPathResource("queriesDrop.sql");
        databasePopulator.setScripts(dropTables);
        databasePopulator.execute(dataSource);
    }

    @Test
    public void getByIdTest() {
        assertEquals(account, accountRepository.findOne(1));
    }

    @Test
    public void getAllTest() {
        List<Account> accountsDao = accountRepository.findAll();
        int i = 0;
        assertEquals(accounts.size(), accountsDao.size());
        for (Account account : accounts) {
            assertEquals(account, accountsDao.get(i));
            i++;
        }
    }

    @Test
    public void getByEmailTest() {
        assertEquals(account, accountRepository.findByEmail("ivanov@mail.ru"));
    }

    @Transactional
    @Test
    public void deleteTest() {
        accountRepository.delete(account);
        assertNull(accountRepository.findOne(account.getId()));
    }

    @Test
    public void updateTest() {
        account.setHomeAddress("New home address");
        accountRepository.save(account);
        assertEquals(account, accountRepository.findOne(1));
    }

    @Test
    public void insertTest() throws ParseException {
        Account newAccount = new Account("Petrov", "Petr", "Ivanovich",
                new SimpleDateFormat("dd.MM.yyyy").parse("10.05.1990"), "petrov123@mail.ru", "123445",
                'm',"home address", "work address",
                "@petro","about me");
        newAccount = accountRepository.save(newAccount);
        assertEquals(newAccount, accountRepository.findOne(3));
    }

    @Test
    public void searchAccountsTest() {
        List<Account> result = accountRepository.findAllAccountsByPattern("Pet", new PageRequest(0, Integer.MAX_VALUE));
        assertEquals(result.get(0), friend);
    }

    @Test
    public void findAllAccountsByPatternTest() throws ParseException {
        Account newAccount1 = new Account();
        Account newAccount2 = new Account();
        newAccount1.setLastName("Petrov");
        newAccount1.setFirstName("Serg");
        newAccount1.setEmail("petrovich@mail.com");
        newAccount1.setPassword("123");
        newAccount2.setLastName("Krasinov");
        newAccount2.setFirstName("Serg");
        newAccount2.setEmail("serg@mail.com");
        newAccount2.setPassword("123");

        accountRepository.save(newAccount1);
        accountRepository.save(newAccount2);

        List<Account> result = accountRepository.findAllAccountsByPattern("ov", new PageRequest(1, 2));
        assertEquals(2, result.size());
        result = accountRepository.findAllAccountsByPattern("ov", new PageRequest(0, 3));
        assertEquals(3, result.size());
    }

    @Test
    public void countAccountsByPatternTest() {
        Account newAccount1 = new Account();
        Account newAccount2 = new Account();
        newAccount1.setLastName("Petrov");
        newAccount1.setFirstName("Serg");
        newAccount1.setEmail("petrovich@mail.com");
        newAccount1.setPassword("123");
        newAccount2.setLastName("Krasinov");
        newAccount2.setFirstName("Serg");
        newAccount2.setEmail("serg@mail.com");
        newAccount2.setPassword("123");

        accountRepository.save(newAccount1);
        accountRepository.save(newAccount2);

        int result = accountRepository.countAccountsByPattern("ov");
        assertEquals(4, result);
    }

    @Test
    public void getAvatarById() {
        byte[] array = new BigInteger("1111000011110001", 2).toByteArray();
        account.setAvatar(array);
        Account accountNew = accountRepository.findOne(account.getId());
        assertArrayEquals(account.getAvatar(), accountNew.getAvatar());
    }

    @Test
    public void getAllPhonesTest() {
        Account account = accountRepository.findOne(1);
        List<Phone> phonesFromDB = account.getPhones();
        int i = 0;
        assertEquals(phones.size(), phonesFromDB.size());
        for (Phone phone : phones) {
            assertEquals(phone, phonesFromDB.get(i));
            i++;
        }
    }

    @Test
    public void updatePhoneTest() {
        phone.setPhone("9080000000");
        account.addPhone(phone);
        assertEquals(phone, (accountRepository.findOne(account.getId())).getPhones().get(0));
    }

    @Test
    public void insertPhoneTest() {
        Phone newPhone = new Phone("9030000000", account);
        account.addPhone(newPhone);
        assertTrue((accountRepository.findOne(account.getId())).getPhones().contains(newPhone));
    }

    @Test
    public void getAllPhonesByIdAccountTest() {
        Phone newPhone = new Phone("9030000000", account);
        account.addPhone(newPhone);
        assertEquals(2, (accountRepository.findOne(account.getId())).getPhones().size());
        assertTrue((accountRepository.findOne(account.getId())).getPhones().contains(phone) &&
                (accountRepository.findOne(account.getId())).getPhones().contains(newPhone));

    }

    @Test
    public void getRequestFromTest() throws ParseException {
        assertEquals(0, accountRepository.findOne(account.getId()).getRequestsFrom().size());
        assertEquals(0, accountRepository.findOne(friend.getId()).getRequestsTo().size());
        account.addRequestsFromMe(friend);
        assertEquals(1, accountRepository.findOne(account.getId()).getRequestsFrom().size());
        assertEquals(1, accountRepository.findOne(friend.getId()).getRequestsTo().size());
    }
}
