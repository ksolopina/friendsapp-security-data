<%@ include file="common/header.jsp" %>
<nav class="navbar navbar-light">
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav nav-friends" id="nav-friends">
            <a class="nav-item nav-link"
               href="${pageContext.request.contextPath}/showFriends?accId=${account.id}&countFriends=${countFriends}&countFollowers=${countFollowers}&countRecipients=${countRecipients}">
                Друзья <span class="count" id="countFriends">${countFriends}</span></a>
            <a class="nav-item nav-link"
               href="${pageContext.request.contextPath}/showFollowers?accId=${account.id}&countFriends=${countFriends}&countFollowers=${countFollowers}&countRecipients=${countRecipients}">
                Подписчики <span class="count" id="countFollowers">${countFollowers}</span></a>
            <a class="nav-item nav-link"
               href="${pageContext.request.contextPath}/showRecipients?accId=${account.id}&countFriends=${countFriends}&countFollowers=${countFollowers}&countRecipients=${countRecipients}">
                Отправленные запросы <span class="count" id="countRecipients">${countRecipients}</span></a>
        </div>
    </div>
</nav>



<div class="friends-wrapper">
    <c:set var="index" value="0"/>
    <c:forEach items="${accounts}" var="account">
        <div class="friends-block" id="blockSearch${index}">
            <div class="profile-picture avatar-small">
                <c:choose>
                    <c:when test="${account.avatar != null}">
                        <img id="avatar${index}" class="editable img-responsive avatar" src="data:image/jpeg;base64,${account.avatar}"/>
                    </c:when>
                    <c:otherwise>
                        <img id="avatar${index}" src="${pageContext.request.contextPath}/img/avatar.png"
                             class="editable img-responsive avatar">
                    </c:otherwise>
                </c:choose>
            </div>
            <div>
                <a href="${pageContext.request.contextPath}/account/${account.id}" id="link${index}">${account.firstName} ${account.lastName}</a>
            </div>
        </div>
        <c:set var="index" value="${index + 1}"/>
    </c:forEach>
</div>
<%@ include file="common/footer.jsp" %>